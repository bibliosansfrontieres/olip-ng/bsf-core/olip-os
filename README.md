# OLIP OS

Create a custom image for OLIP deployments.

## Setup

Download a RaspberryPi OS image.
The file path shall be set as `RPIOS_IMAGE_FILE`
latter in the `.env` file.

```shell
cd ~/Documents/isos/raspbian/

wget https://downloads.raspberrypi.com/raspios_lite_arm64/images/raspios_lite_arm64-2024-07-04/2024-07-04-raspios-bookworm-arm64-lite.img.xz

xz -d 2024-07-04-raspios-bookworm-arm64-lite.img.xz
```

Install `pishrink`:

```shell
wget https://raw.githubusercontent.com/Drewsif/PiShrink/master/pishrink.sh

chmod +x pishrink.sh  

sudo mv pishrink.sh /usr/local/bin
```

Configure the script:

```shell
cp .env.example .env
vi .env
```

Get a SD card - it should be as small as possible as we need to copy it later.

## Quick start

Once everything has been setup:

Flash a new sd card:

```shell
sudo ./flash-raspberrypios.sh
```

Boot a RPi with it, ssh into it, run the `customize.sh` script:

```shell
./customize.sh
```

Poweroff the RPi, bring the SD card back to your laptop,
create the final OLIP-os image:

```shell
# TODO/FIXME this script doesn't read the .env file yet
create-olip-os-image.sh -d /dev/mmcblk0
```

Enjoy the result:

```text
ls -l -h *.img
-rw-r--r-- 1 tom  tom  6,8G 2024-09-13 18:06 olipos-20240913175805-sdcard-helpers-2855661-DIRTY.img
```

## More details

### flash-raspberrypios.sh

This script does the following operations:

- flash the configured RaspberryPi OS image to the SD card
- rename `pi` user to `ideascube`
- set `ideascube` user password to `ideascube`
- add our SSH keys to `~root` and `~ideascube` users
- enable `sshd`
- copy the `customize.sh` script to `/root/customize.sh`
- writes the script's version information to `/etc/olipos_version`

Example run output:

```text
$ sudo ./flash-raspberrypios.sh
[flash-raspberrypios.sh] About to flash
the Image:      /home/tom/Documents/isos/raspbian/2024-07-04-raspios-bookworm-arm64-lite.img
to Device:      /dev/mmcblk0
with version:   2855661 @ sdcard-helpers
in 3 seconds...
[flash-raspberrypios.sh] Flash image...
[flash-raspberrypios.sh] Install SSH keys...
[flash-raspberrypios.sh] Copy customize.sh script...
[flash-raspberrypios.sh] Enable SSHd...
[flash-raspberrypios.sh] Done.
```

Note that the 3 seconds delay before actually flashing the card allows you to `^C`,
and is extended to 10 seconds when the repository is in a dirty state.

Example generated `/etc/olipos_version` file (with comments):

```shell
OLIPOS_DATE=20240913172148      # ./flash-raspberrypios.sh` run timestamp
OLIPOS_BRANCH=sdcard-helpers    # olip-os repository branch
OLIPOS_COMMIT=2855661           # olip-os repository commit
OLIPOS_DIRTY=false              # olip-os repository state
```

## create-olip-os-image.sh

This script does the following operations:

- dump the SD card to an image file - taggerd as `LARGE`
- shrink the image filesystem
- delete the `LARGE` image - if configured to do so
- set ownership back to your `$UID`

```text
$ sudo ./create-olip-os-image.sh -d /dev/mmcblk0
[create-olip-os-image.sh] Copying /dev/mmcblk0 to olipos-20240913175805-sdcard-helpers-2855661-DIRTY_LARGE.img
31849148928 octets (32 GB, 30 GiB) copiés, 437 s, 72,9 MB/s
62333952+0 enregistrements lus
62333952+0 enregistrements écrits
31914983424 octets (32 GB, 30 GiB) copiés, 437,877 s, 72,9 MB/s
[create-olip-os-image.sh] Shrinking olipos-20240913175805-sdcard-helpers-2855661-DIRTY_LARGE.img to olipos-20240913175805-sdcard-helpers-2855661-DIRTY.img
pishrink.sh v0.1.4
pishrink.sh: Copying olipos-20240913175805-sdcard-helpers-2855661-DIRTY_LARGE.img to olipos-20240913175805-sdcard-helpers-2855661-DIRTY.img... ...
pishrink.sh: Gathering data ...
Creating new /etc/rc.local
pishrink.sh: Checking filesystem ...
rootfs: Inode 59617 extent tree (at level 1) could be narrower.  IGNORED.
rootfs: 143140/1823328 files (0.1% non-contiguous), 1296926/7659648 blocks
resize2fs 1.47.0 (5-Feb-2023)
pishrink.sh: Shrinking filesystem ...
resize2fs 1.47.0 (5-Feb-2023)
Resizing the filesystem on /dev/loop0 to 1641978 (4k) blocks.
Begin pass 2 (max = 55082)
Relocating blocks             XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
Begin pass 3 (max = 234)
Scanning inode table          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
Begin pass 4 (max = 16446)
Updating inode references     XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
The filesystem on /dev/loop0 is now 1641978 (4k) blocks long.

pishrink.sh: Shrinking image ...
pishrink.sh: Shrunk olipos-20240913175805-sdcard-helpers-2855661-DIRTY.img from 30G to 6.8G ...
[create-olip-os-image.sh] Deleting olipos-20240913175805-sdcard-helpers-2855661-DIRTY_LARGE.img
[create-olip-os-image.sh] Fix ownership/filemodes
```

Note that the resulting image file naming
is templated from `/etc/olipos_version` information:

```shell
olipos-$TIMESTAMP-$BRANCH-$COMMIT-$DIRTYSTATE.img
```

In this example, it was considered `DIRTY` because I edited this very documentation
after running `./flash-raspberrypios.sh`.
