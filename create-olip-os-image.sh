#!/bin/bash

if [[ ! $EUID = 0 ]]; then
    echo >&2 "This script requires root privileges. Please use sudo."
    exit 3
fi

COMPRESS_FINAL_IMAGE=false
KEEP_TEMP_IMAGE=false

# Source configuration file
# shellcheck source=/dev/null
[ -r ./.env ] && . ./.env


say() {
    echo >&2 "[$( basename "$0")] $*"
}
show_usage() {
    echo >&2 "Usage: $0 [-c] [-d DEVICE] [-k]"
    echo >&2 "Example: $0 -d /dev/mmcblk0 -k"
    echo >&2 "-c    Compress final image after shrinking (use gzip) (COMPRESS_FINAL_IMAGE)"
    echo >&2 "-d    The source SDCard Device to dump from (SDCARD_DEVICE)"
    echo >&2 "-k    Keep the temporary (large) image file after shrinking (KEEP_TEMP_IMAGE)"
}
check_available_space() {
    local_filesystem=$(df --output=source "$(readlink -f "$0")" | tail -n 1)
    if [ ! -e "$local_filesystem" ]; then
        say "Error: Target device file '$local_filesystem' does not exist."
        exit 1
    fi
    # Get total size of source device
    sdcard_size=$(blockdev --getsize64 "$SDCARD_DEVICE")
    # Get available space on target device
    target_available_space=$(df --output=avail -B 1 "$local_filesystem" | tail -n 1)
    # Check if available space is greater than or equal to source total size
    if [ ! "$target_available_space" -ge "$sdcard_size" ]; then
        say "Error: There is not enough space on $local_filesystem to copy $SDCARD_DEVICE."
        exit 1
    fi
}
validate_device() {
    if [ ! -e "$SDCARD_DEVICE" ]; then
        say "Error: Device $SDCARD_DEVICE does not exist"
        exit 1
    fi
}
mount_sdcard(){
    TMP_MOUNTPOINT="$(mktemp -d mountpoint_XXXXX)"
    mount "${SDCARD_DEVICE}p2" "$TMP_MOUNTPOINT"
}
umount_sdcard(){
    until umount "$TMP_MOUNTPOINT" 2>/dev/null && rmdir "$TMP_MOUNTPOINT" ; do sleep 1 ; done
}
get_version(){
    if [ -r "${TMP_MOUNTPOINT}/etc/olipos_version" ] ; then
        # shellcheck source=/dev/null
        . "${TMP_MOUNTPOINT}/etc/olipos_version"
    else
        say "$SDCARD_DEVICE does not look like an OLIP OS image: /etc/olipos_version not found."
        notolip=true
    fi
    [ "$notolip" ] && umount_sdcard && exit 1
    VERSION="${OLIPOS_BRANCH}-${OLIPOS_COMMIT}"
    [ "$OLIPOS_DIRTY"  == "true" ] && VERSION="${VERSION}-DIRTY"
}
filesystem_cleanup(){
    say "Filesystem cleanup"
    set -u
    rm -rf \
        "${TMP_MOUNTPOINT}/var/log/*" \
        "${TMP_MOUNTPOINT}/var/swap"
    set +u
}
build_file_names(){
    local ts
    ts="$( date +%Y%m%d%H%M%S )"
    local prefix="olipos-${ts}-${VERSION}"
    local ext='img'
    TEMP_IMAGE="${prefix}_LARGE.${ext}"
    FINAL_IMAGE="${prefix}.${ext}"
}
dump_sdcard() {
    say "Copying $SDCARD_DEVICE to $TEMP_IMAGE"
    dd if="$SDCARD_DEVICE" of="$TEMP_IMAGE" status=progress
}
shrink_image() {
    say "Shrinking $TEMP_IMAGE to $FINAL_IMAGE"
    [ "$COMPRESS_FINAL_IMAGE" = true ] && pishrink_opt_compress='-z'
    # shellcheck disable=SC2086
    pishrink.sh $pishrink_opt_compress "$TEMP_IMAGE" "$FINAL_IMAGE"
    if [ "$KEEP_TEMP_IMAGE" = false ]; then
        say "Deleting $TEMP_IMAGE"
        rm "$TEMP_IMAGE"
    fi
}
make_image_files_readable(){
    say "Fix ownership/filemodes"
    if [ "$SUDO_UID" ] ; then
        # shellcheck disable=SC2153
        chown "${SUDO_UID}:${SUDO_GID}" "$FINAL_IMAGE"
    fi
    chmod a+r "$FINAL_IMAGE"
}

main() {

    if [ -z "$1" ] ; then show_usage ; exit 1 ; fi

    # Parse options
    while getopts "cd:hk" opt; do
        case $opt in
            c)  COMPRESS_FINAL_IMAGE=true;;
            d)  SDCARD_DEVICE=$OPTARG ;;
            k)  KEEP_TEMP_IMAGE=true;;
            h)  show_usage ; exit 0 ;;
            \?) echo >&2 "Invalid option: -$OPTARG"; exit 1;;
        esac
    done

    validate_device
    check_available_space
    mount_sdcard
    get_version
    filesystem_cleanup
    umount_sdcard
    build_file_names
    dump_sdcard
    shrink_image
    make_image_files_readable
}

main "$@"
