#!/bin/bash

if [[ ! $EUID = 0 ]]; then
    echo >&2 "You must be root."
    exit 3
fi

say() {
    echo >&2 "[$( basename "$0")] $*"
}

set -eo pipefail


# Source configuration file
# shellcheck source=/dev/null
[ -r ./.env ] && . ./.env 

# RPIOS image: args vs conf
if [ -n "$1" ] ; then
    IMG="$1"
elif [ -n "$RPIOS_IMAGE_FILE" ] ; then
    IMG="$RPIOS_IMAGE_FILE"
else
    say "Error: no image set."
    exit 1
fi
[ -r "$IMG" ] || { say "Error: image file not readable: $IMG" ; exit 1 ; }

# SDCard device: args vs conf
if [ -n "$2" ] ; then
    DEV="$2"
elif [ -n "$SDCARD_DEVICE" ] ; then
    DEV="$SDCARD_DEVICE"
else
    say "Error: no SDCard device set."
    exit 1
fi
file "$DEV" | grep -F -q 'block special' || { say "Error: invalid device: $DEV" ; exit 1 ; }



# Get some git information
BRANCH="$( git rev-parse --abbrev-ref HEAD )"
COMMIT="$( git rev-parse --short HEAD )"
if ! git diff-index --quiet HEAD -- ; then
    DIRTY_STATE=true ; DIRTY_TAG="(DIRTY)" DELAY_BEFORE_FLASH=10
else
    DIRTY_STATE=false ; DIRTY_TAG='' ; DELAY_BEFORE_FLASH=3
fi



say "About to flash
the Image:      $IMG
to Device:      $DEV
with version:   $COMMIT @ $BRANCH $DIRTY_TAG
in $DELAY_BEFORE_FLASH seconds..."

sleep "$DELAY_BEFORE_FLASH"



say "Flash image..."
cat "$IMG" > "$DEV"

TMP_MOUNTPOINT="$(mktemp -d .mountpoint_XXXXX)"
mount "${DEV}p2" "${TMP_MOUNTPOINT}"

if [ -r /root/.ssh/authorized_keys ] ; then
    say "Install SSH keys..."
    echo "# Added at RaspberyPiOS flash stage"  >  "${TMP_MOUNTPOINT}/root/.ssh/authorized_keys"
    cat /root/.ssh/authorized_keys              >> "${TMP_MOUNTPOINT}/root/.ssh/authorized_keys"
    echo                                        >> "${TMP_MOUNTPOINT}/root/.ssh/authorized_keys"
fi

say "Copy customize.sh script..."
cp ./customize.sh "${TMP_MOUNTPOINT}/root/"
echo "OLIPOS_DATE=$( date +%Y%m%d%H%M%S )
OLIPOS_BRANCH=$BRANCH
OLIPOS_COMMIT=$COMMIT
OLIPOS_DIRTY=$DIRTY_STATE" > "${TMP_MOUNTPOINT}/etc/olipos_version"
chmod a+r "${TMP_MOUNTPOINT}/etc/olipos_version"

sleep 1
umount "${TMP_MOUNTPOINT}"



mount "${DEV}p1" "${TMP_MOUNTPOINT}"

# /lib/systemd/system/sshswitch.service && /usr/lib/raspberrypi-sys-mods/sshswitch
say "Enable SSHd..."
touch "${TMP_MOUNTPOINT}/ssh"

sleep 1
umount "${TMP_MOUNTPOINT}"
rmdir "${TMP_MOUNTPOINT}"



say "Done."
