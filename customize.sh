#!/bin/bash

if [ "$EUID" -ne 0 ]
    then echo "Error: Please run as root!"
    exit 1
fi

say() {
    echo >&2 "[$(basename "$0")] $*"
    echo "$*" | systemd-cat -t "$(basename "$0")" -p info
}

# stop on errors
set -euo pipefail

# The branch to use for deploy.sh stage
DEPLOY_BRANCH="${DEPLOY_BRANCH:-main}"

# The tag used for OLIP images
TAG="${TAG:-latest}"

YQ_BINARY="${YQ_BINARY:-yq_linux_arm64}"
YQ_VERSION="${YQ_VERSION:-v4.44.5}"

DEPLOY_PATH=/home/ideascube/deploy
ROOTS="alexandre audrey tom"
USER='ideascube'
PASS='ideascube'

export DEBIAN_FRONTEND=noninteractive 

say "Nuke userconf service to free the dpkg lock"
systemctl stop userconfig.service
apt-get purge --auto-remove --yes --quiet --quiet \
    userconf-pi

say "Install dependencies"
apt-get update --quiet --quiet
apt-get upgrade --yes --quiet --quiet
apt-get install --yes --quiet --quiet \
    git \
    ifupdown \
    iptables-persistent \
    eject-
debconf-set-selections <<<"iptables-persistent iptables-persistent/autosave_v4 boolean true
iptables-persistent iptables-persistent/autosave_v6 boolean true"

say "Cgroups fix for docker 'memory limits' in RPiOS"
sed -i -e 's/$/   cgroup_enable=memory swapaccount=1 cgroup_memory=1 cgroup_enable=cpuset/' /boot/firmware/cmdline.txt

say "Set WIFI to FR"
raspi-config nonint do_wifi_country FR

say "Use the minimal version for wifi driver"
update-alternatives --set cyfmac43455-sdio.bin /lib/firmware/cypress/cyfmac43455-sdio-minimal.bin

say "Remove wlan0 management by NetworkManager"
printf "[keyfile]\nunmanaged-devices=interface-name:wlan0" > /etc/NetworkManager/conf.d/wlan0.conf
systemctl restart NetworkManager

say "Fix IP address for wlan0"
printf "auto wlan0\niface wlan0 inet static\n address 10.3.141.1\n netmask 255.255.255.0\n" >> /etc/network/interfaces.d/wlan0.conf
systemctl restart networking

say "Internet forwarding from eth0 to wlan0"
sed -i 's/^#\(net.ipv4.ip_forward=1\)/\1/' /etc/sysctl.conf
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
iptables-save > /etc/iptables/rules.v4
# FIxME: there is nothing to save here - yet?
ip6tables-save > /etc/iptables/rules.v6

say "Install PiSugar"
PISUGAR_SERVER_FILE="pisugar-server_1.7.6-1_arm64.deb"
PISUGAR_POWEROFF_FILE="pisugar-poweroff_1.7.6-1_arm64.deb"
curl -fsSL -o "$PISUGAR_SERVER_FILE" "http://cdn.pisugar.com/release/${PISUGAR_SERVER_FILE}"
curl -fsSL -o "$PISUGAR_POWEROFF_FILE" "http://cdn.pisugar.com/release/${PISUGAR_POWEROFF_FILE}"
debconf-set-selections <<< "pisugar-server pisugar-server/model select PiSugar 3
pisugar-server pisugar-server/auth-username string admin
pisugar-server pisugar-server/auth-password password admin
pisugar-poweroff pisugar-poweroff/model select PiSugar 3"
dpkg -i "$PISUGAR_SERVER_FILE" "$PISUGAR_POWEROFF_FILE"
rm "$PISUGAR_SERVER_FILE" "$PISUGAR_POWEROFF_FILE"

say "Create or rename user"
if getent passwd 'pi' &> /dev/null ; then
    usermod -l "$USER" pi --home "/home/$USER" --move-home
    groupmod --new-name "$USER" pi
    mv /etc/sudoers.d/010_pi-nopasswd "/etc/sudoers.d/010_${USER}-nopasswd"
    sed -i -e 's/^pi /ideascube /' "/etc/sudoers.d/010_${USER}-nopasswd"
else
    useradd -m "$USER"
    echo "$USER ALL=(ALL) NOPASSWD: ALL" > "/etc/sudoers.d/010_${USER}-nopasswd"
fi
say "Set user password"
echo "${USER}:${PASS}" | chpasswd

say "Add authorized_keys"
echo "# Added at customize.sh stage" >> /root/.ssh/authorized_keys
for user in $ROOTS ; do
    curl -s "https://s3.eu-central-1.wasabisys.com/dropcube/sshkeys/${user}.pub" \
        >> /root/.ssh/authorized_keys
done
echo >> /root/.ssh/authorized_keys
cp -a /root/.ssh/ "/home/${USER}/"
chown -R "${USER}:${USER}" "/home/${USER}/.ssh/"

say "Ensure SSHd is enabled"
systemctl enable ssh.service

say "Install macswitcher service"
git clone https://gitlab.com/bibliosansfrontieres/olip-ng/bsf-core/macswitcher.git /usr/local/src/macswitcher
cd /usr/local/src/macswitcher
bash ./install.sh

say "Install netprobe service"
git clone https://gitlab.com/bibliosansfrontieres/olip-ng/bsf-core/netprobe.git /usr/local/src/netprobe
cd /usr/local/src/netprobe
bash ./install.sh
sed -i -e 's@^NETPROBE_FLAGS_DIR=.*@NETPROBE_FLAGS_DIR=/olip-files/network@' /etc/default/netprobe

say "Install Docker"
curl -fsSL https://get.docker.com/ -o- | bash -
usermod -aG docker ideascube

say "Install yq"
wget -q "https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/${YQ_BINARY}.tar.gz" -O- \
    | tar -xzf - "./${YQ_BINARY}"
mv "./${YQ_BINARY}" /usr/local/bin/yq

say "Clone OLIP's deploy repository (branch=${DEPLOY_BRANCH})"
git clone --branch "$DEPLOY_BRANCH" https://gitlab.com/bibliosansfrontieres/olip-ng/bsf-core/deploy.git "$DEPLOY_PATH"
cd "$DEPLOY_PATH"
mkdir -p /olip-files/install
echo "${DEPLOY_BRANCH}" > /olip-files/install/deploy-branch
git rev-parse --short HEAD > /olip-files/install/deploy-commit

say "Run OLIP install"
bash scripts/install.sh -t "$TAG"

say "Change hostname"
raspi-config nonint do_hostname olipos

say "Set timezone to UTC"
raspi-config nonint do_change_timezone Etc/UTC

say "Trigger systemd machine initialization on next boot"
: > /etc/machine-id

say "Do some cleanup: apt packages"
apt-get purge --yes --quiet --quiet apt-listchanges
say "Do some cleanup: apt cache"
apt-get clean && rm -rf /var/lib/apt/lists/* /var/cache/apt/*
say "Do some cleanup: backups"
find /var/backups -type f -delete
say "Do some cleanup: disable some more systemd units"
systemctl mask --now sshswitch.service bluetooth.service bluetooth.target man-db.timer alsa-restore.service

say "Remove executable bit to avoid re-running the script"
chmod a-x "/root/customize.sh"

say "Done. It's probably time to shutdown -h now"
